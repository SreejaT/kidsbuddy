import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor
} from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable()
export class JwtInterceptor implements HttpInterceptor {

  constructor() {}

  intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {
    request = request.clone({
      setHeaders: {
        Authorization: `Bearer eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJ4eXpAaW5kYnl0ZXMuY29tIiwidXNlclJvbGUiOiIxIiwiZXhwIjoxNjQyNTY5OTMzLCJ1c2VySWQiOjY5MiwiaWF0IjoxNjQwNzY5OTMzfQ.UumkBqHdVL3L_alRIHgbTeKlfOEnXlTr7uUi73jG1nM`
      }
    })
    return next.handle(request);
  }
}
