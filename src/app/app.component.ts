import { Component, OnInit } from '@angular/core';
import { CommonService } from './common.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'kidsbuddy';
  Response:any;
  constructor(private common:CommonService){}
ngOnInit(){
  this.common.getData().subscribe(response=>{
    this.Response = response;
  })
}

}
