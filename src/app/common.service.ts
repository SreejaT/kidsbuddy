import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class CommonService {

  constructor(private http:HttpClient) { }
  getData(){
    return this.http.get('https://sfv6kakwjb.execute-api.ap-south-1.amazonaws.com/dev/smiley_admin/spelling-bee/viewAll');
  }
}
